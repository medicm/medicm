### Hi there 👋

![Dino](https://raw.githubusercontent.com/medicm/medicm/master/dino.gif)


**📫  Reach me at:**


[w7vx4sihw@relay.firefox.com](mailto:w7vx4sihw@relay.firefox.com)
[![Linkedin Badge](https://img.shields.io/badge/-LinkedIn-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/mmedic/)](https://www.linkedin.com/in/mmedic)
